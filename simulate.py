#!/usr/bin/env python

"""CT simulations"""

__author__ = "Zack Scholl"
__copyright__ = "Copyright 2016, Duke University"
__credits__ = ["Zack Scholl"]
__license__ = "None"
__version__ = "0.1"
__maintainer__ = "Zack Scholl"
__email__ = "zns@duke.edu"
__status__ = "Production"

import time
import json
import os
import sys
import shutil
import subprocess
import shlex
import logging
import glob

# set up logging to file - see previous section for more details
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M:%S',
                    filename='simulation.log',
                    filemode='w')
# define a Handler which writes INFO messages or higher to the sys.stderr
console = logging.StreamHandler()
console.setLevel(logging.INFO)
# set a format which is simpler for console use
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
# tell the handler to use this format
console.setFormatter(formatter)
# add the handler to the root logger
logging.getLogger('').addHandler(console)

baseDir = os.getcwd()

from oxt import *
from conf import *
from alignment import *
from dump import *


def initiate():
    logger = logging.getLogger("initiate")
    # Load amino acid translator
    trans = dict((s, l.upper()) for l, s in [
                 row.strip().split() for row in open("table").readlines()])

    # Load fasta seqeuence
    src = ""
    for line in params['fasta'].splitlines():
        if ">" in line:
            continue
        src = src + line.strip()

    # Convert 1-letter code to 3-letter code
    src = src.upper()
    seq = []
    for s in list(src):
        seq.append(trans[s])

    params['fullSequence'] = ' '.join(seq)

    logger.debug("Generate linear seqeuence, fullSequenceLinear.pdb")
    tleapConf = """%s
loadAmberParams frcmod.ionsjc_tip3p
mol = sequence { %s }
saveamberparm mol prmtop inpcrd
quit""" % (params['proteinff'], params['fullSequence'])
    with open("tleap.foo", "w") as f:
        f.write(tleapConf)
    cmd = "tleap -f tleap.foo"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.system("$AMBERHOME/bin/ambpdb -p prmtop < inpcrd > fullSequenceLinear.pdb")
    os.remove("prmtop")
    os.remove("inpcrd")
    os.remove("tleap.foo")

    logger.debug("Writing the configuration files")
    for f in files:
        with open(f, 'w') as f2:
            f2.write(files[f])
    print(json.dumps(params, indent=2))
    with open('params.json', 'w') as f:
        f.write(json.dumps(params, indent=2))


def firstSimulation():
    logger = logging.getLogger("firstSimulation")
    newFolder = str(params['chopPoints'][0])
    logger = logging.getLogger('simulate-' + newFolder + 'aa')
    logger.info("Starting first simulation at chop point " +
                str(params['chopPoints'][0]))
    logger.info("Creating prmtop/inpcrd from sequence")
    partialSequence = " ".join(params['fullSequence'].split()[
                               :params['chopPoints'][0]])
    tleapConf = """%s
loadAmberParams frcmod.ionsjc_tip3p
mol = sequence { %s NHE }
solvatebox mol TIP3PBOX 8.0
addions mol Na+ 0
addions mol Cl- 0
saveamberparm mol prmtop inpcrd
quit""" % (params['proteinff'], partialSequence)
    try:
        shutil.rmtree(newFolder)
    except:
        pass
    os.system('rm -rf %s' % newFolder)
    os.mkdir(newFolder)
    os.chdir(newFolder)
    with open("tleap.foo", "w") as f:
        f.write(tleapConf)
    cmd = "tleap -f tleap.foo"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.remove("tleap.foo")

    if params['reweight']:
        logger.info("Re-weighting hydrogens")
        reweightHydrogens()

    # logger.info("Removing OXT charge...")
    # removeCharge()
    os.system("%(amber)s/bin/ambpdb -p prmtop < inpcrd > startingConfiguration.pdb" %
              {'amber': os.environ['AMBERHOME']})

    with open("reimage.in", "w") as f:
        f.write("""trajin 03_Prod.mdcrd
trajout 03_Prod_reimage.mdcrd
center :1-%(last)s
image familiar
go""" % {'last': str(params['chopPoints'][0])})

    runSimulation(str(params['chopPoints'][0]))


def runSimulation(nn):
    logger = logging.getLogger('simulate-' + nn + 'aa')
    logger.info("Minimizing...")
    os.system('cp ../01_Min.in ./')
    cmd = "%(amber)s/bin/pmemd.cuda -O -i %(cwd)s/01_Min.in -o %(cwd)s/01_Min.out -p %(cwd)s/prmtop -c %(cwd)s/inpcrd -r %(cwd)s/01_Min.rst -inf %(cwd)s/01_Min.mdinfo" % {
        'cwd': os.getcwd(), 'amber': os.environ['AMBERHOME']}
    proc = subprocess.Popen(shlex.split(cmd), shell=False, env=dict(
        os.environ, CUDA_VISIBLE_DEVICES=params['cuda']))
    proc.wait()

    logger.info("Heating to %d..." % params['temp'])
    os.system('cp ../02_Heat.in ./')
    cmd = "%(amber)s/bin/pmemd.cuda -O -i %(cwd)s/02_Heat.in -o %(cwd)s/02_Heat.out -p %(cwd)s/prmtop -c %(cwd)s/01_Min.rst -r %(cwd)s/02_Heat.rst -x %(cwd)s/02_Heat.mdcrd -inf %(cwd)s/02_Heat.mdinfo" % {
        'cwd': os.getcwd(), 'amber': os.environ['AMBERHOME']}
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False, env=dict(
        os.environ, CUDA_VISIBLE_DEVICES=params['cuda']))
    proc.wait()

    logger.info("Pre-production simulation...")
    os.system('cp ../025_PreProd.in ./')
    cmd = "%(amber)s/bin/pmemd.cuda -O -i %(cwd)s/025_PreProd.in -o %(cwd)s/025_PreProd.out -p %(cwd)s/prmtop -c %(cwd)s/02_Heat.rst -r %(cwd)s/025_PreProd.rst -x %(cwd)s/025_PreProd.mdcrd -inf %(cwd)s/025_PreProd.mdinfo" % {
        'cwd': os.getcwd(), 'amber': os.environ['AMBERHOME']}
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False, env=dict(
        os.environ, CUDA_VISIBLE_DEVICES=params['cuda']))
    proc.wait()
    logger.info("Simulation finished.")

    logger.info("Production simulation...")
    os.system('cp ../03_Prod.in ./')
    cmd = "%(amber)s/bin/pmemd.cuda -O -i %(cwd)s/03_Prod.in -o %(cwd)s/03_Prod.out -p %(cwd)s/prmtop -c %(cwd)s/025_PreProd.rst -r %(cwd)s/03_Prod.rst -x %(cwd)s/03_Prod.mdcrd -inf %(cwd)s/03_Prod.mdinfo" % {
        'cwd': os.getcwd(), 'amber': os.environ['AMBERHOME']}
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False, env=dict(
        os.environ, CUDA_VISIBLE_DEVICES=params['cuda']))
    # Poll process until finished
    step = 0
    while True:
        if proc.poll() != None:
            break
        time.sleep(10)
        step += 1
        if step % 60 == 0:
            os.system(
                "grep 'ns/day' %(cwd)s/03_Prod.mdinfo | tail -n 1 | awk '{print $4}' > foo1" % {'cwd': os.getcwd()})
            os.system(
                """grep 'time rem' %(cwd)s/03_Prod.mdinfo | tail -n 1 | awk '{print $5" "$6}' > foo2"""  % {'cwd': os.getcwd()})
            speed = open('foo1', 'r').read().strip()
            timeleft = open('foo2', 'r').read().strip()
            logger.debug("ns/day: %s, time left: %s" % (speed, timeleft))
            os.remove('foo1')
            os.remove('foo2')
    logger.info("Simulation finished.")

    logger.info("Reimaging...")
    cmd = "%(amber)s/bin/cpptraj -p %(cwd)s/prmtop -i %(cwd)s/reimage.in" % {
        'cwd': os.getcwd(), 'amber': os.environ['AMBERHOME']}
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    logger.info("Reimaging finished.")

    logger.info("Dumping to pdbs/")
    dumpPDBs()  # dump.py
    logger.info("Finished dumping pdbs.")

    logger.info("Calculating ss..")
    determineSS()  # dump.py
    logger.info("Finished calculating ss.")

    logger.info("Calculating alignment...")
    calculateAlignment()
    logger.info("Finished calculating alignment.")


def simulate(n, isLast=False):
    os.chdir(baseDir)
    if n == 0:
        initiate()
        firstSimulation()
    else:
        simulateNthCut(n, isLast)


def reweightHydrogens():
    os.system('cp prmtop prmtop.backup')
    os.system('cp inpcrd inpcrd.backup')
    tleapConf = """setOverwrite true
HMassRepartition 3
outparm prmtop inpcrd
go"""
    with open("parmed.foo", "w") as f:
        f.write(tleapConf)
    cmd = "%s -p prmtop -c inpcrd -O -i parmed.foo" % params['parmedprogram']
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.remove("parmed.foo")


def simulateNthCut(n, isLast, maxNumModifier=0):
    os.chdir(baseDir)
    params = json.load(open('params.json', 'r'))
    logger = logging.getLogger(
        'simulate-' + str(params['chopPoints'][n]) + 'aa')
    newFolder = str(params['chopPoints'][n])
    try:
        shutil.rmtree(newFolder)
    except:
        pass
    os.mkdir(newFolder)
    os.chdir(newFolder)
    startPDB = ""
    if params["method"] is "rmsd":
        rmsds = json.load(
            open("../" + str(params['chopPoints'][n - 1]) + "/rmsds.json", 'r'))
        lowestF = ""
        lowestRMSD = 1000
        for f in rmsds:
            if rmsds[f] < lowestRMSD:
                lowestRMSD = rmsds[f]
                lowestF = f
        print(lowestF, lowestRMSD)
        startPDB = lowestF.split("/")[-1]
        os.system(
            'cp ../' + str(params['chopPoints'][n - 1]) + "/" + lowestF + " ./")
    elif params["method"] is "ss":
        f = glob.glob('../' + str(params['chopPoints'][n - 1]) + "/bestSS*")
        startPDB = f[0].split("/")[-1]
        if "bestSS_0_" not in startPDB:
            os.system("cp %s ./" % f[0])
    if "last" in params["method"] or "bestSS_0_" in startPDB:
        logger.debug("Grabbing all pdbs...")
        files = glob.glob(
            "../" + str(params['chopPoints'][n - 1]) + "/pdbs/*.pdb")
        maxNum = 0
        for f in files:
            if "all.pdb" in f:
                continue
            num = int(f.split("/")[-1].replace(".pdb", ""))
            if num > maxNum:
                maxNum = num
        startPDB = "%s.pdb" % str(maxNum + maxNumModifier)
        logger.debug("Max PDB is %s" % startPDB)
        os.system('cp ../%s/pdbs/%s ./' %
                  (str(params['chopPoints'][n - 1]), startPDB))

    logger.info("Using method %s, starting PDB is %s" %
                (params["method"], startPDB))

    newPartialLinearSequence = " ".join(
        params['fullSequence'].split()[:params['chopPoints'][n]])
    if not isLast:
        newPartialLinearSequence = newPartialLinearSequence + " NHE"
    logger.debug("Generate linear seqeuence to align to and add")
    tleapConf = """%s
loadAmberParams frcmod.ionsjc_tip3p
mol = sequence { %s}
saveamberparm mol prmtop inpcrd
quit""" % (params['proteinff'], newPartialLinearSequence)
    with open("tleap.foo", "w") as f:
        f.write(tleapConf)
    cmd = "tleap -f tleap.foo"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.system("$AMBERHOME/bin/ambpdb -p prmtop < inpcrd > partialSequenceLinear.pdb")
    os.remove("prmtop")
    os.remove("inpcrd")
    os.remove("tleap.foo")

    logger.debug("Performing alignment")
    with open('vmddump', 'w') as f:
        f.write("""mol new %(startPDB)s
mol new partialSequenceLinear.pdb
set sel1 [atomselect 0 "backbone and resid %(res1)s to %(res2)s"]
set sel2 [atomselect 1 "backbone and resid %(res1)s to %(res2)s"]
set transformation_matrix [measure fit $sel1 $sel2]
set move_sel [atomselect 0 "all"]
$move_sel move $transformation_matrix
set a [atomselect 0 "protein and noh"]
$a writepdb aligned.pdb
quit""" % {'startPDB': startPDB, 'res1': params['chopPoints'][n - 1] - 3, 'res2': params['chopPoints'][n - 1] - 1})
    cmd = "vmd -dispdev text -e vmddump"
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()

    os.remove("vmddump")
    os.system("cat partialSequenceLinear.pdb >> aligned.pdb")
    switch = False
    lastRes = params['chopPoints'][n - 1]
    newCut = len(newPartialLinearSequence.split()) + 1
    with open("alignedWithLinearSequence.pdb", "w") as g:
        with open("aligned.pdb", "r") as f:
            for line in f:
                if "END" in line[0:4]:
                    switch = True
                    continue
                if "ATOM" in line[0:4]:
                    res = int(line[23:27])
                    if not switch and res <= lastRes:
                        g.write(line)
                    if switch and res >= lastRes and res <= newCut:
                        g.write(line)
                else:
                    g.write(line)
        g.write("END\n")

    with open('vmddump', 'w') as f:
        f.write("""mol new alignedWithLinearSequence.pdb
set sel1 [atomselect 0 "(protein or resname NHE) and noh"]
$sel1 writepdb alignedWithLinearSequenceNoH.pdb
quit""")
    cmd = "vmd -dispdev text -e vmddump"
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.remove("vmddump")

    logger.info("Creating prmtop/inpcrd from sequence")
    tleapConf = """%s
loadAmberParams frcmod.ionsjc_tip3p
mol = loadpdb alignedWithLinearSequenceNoH.pdb
solvatebox mol TIP3PBOX 8.0
addions mol Na+ 0
addions mol Cl- 0
saveamberparm mol prmtop inpcrd
quit""" % params['proteinff']
    with open("tleap.foo", "w") as f:
        f.write(tleapConf)
    cmd = "tleap -f tleap.foo"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    os.remove("tleap.foo")

    if params['reweight']:
        logger.info("Re-weighting hydrogens")
        reweightHydrogens()

    # if not isLast:
    #     logger.info("Removing OXT charge...")
    #     removeCharge()

    os.system("%(amber)s/bin/ambpdb -p prmtop < inpcrd > startingConfiguration.pdb" %
              {'amber': os.environ['AMBERHOME']})

    with open("reimage.in", "w") as f:
        f.write("""trajin 03_Prod.mdcrd
trajout 03_Prod_reimage.mdcrd
center :1-%(last)s
image familiar
go""" % {'last': str(params['chopPoints'][n])})

    try:
        runSimulation(str(params['chopPoints'][n]))
    except:
        # SIMULATION FAILED because of bad start point
        simulateNthCut(n, isLast, maxNumModifier=maxNumModifier - 1)


if __name__ == "__main__":
    for i in range(0, len(params['chopPoints'])):
        simulate(i, isLast=i == len(params['chopPoints']) - 1)
