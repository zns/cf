import json
import os
import glob


# try:
# 	os.remove("dssp")
# except:
# 	pass

# os.system("mkdir dssp")

# for file in glob.glob("./pdbs/*.pdb"):
# 	fileNum = file.split(".pdb")[0].split("/")[-1]
# 	print("Working on %s" % fileNum)
# 	os.system("./bin/dssp-2.0.4-linux-amd64 -i ./pdbs/%(fileNum)s.pdb -o ./dssp/%(fileNum)s.out" % {'fileNum':fileNum})

acsArray = []
for file in glob.glob("./dssp/*.out"):
	with open(file,"r") as f:
		acs = -1
		for line in f:
			if "ACCESSIBLE SURFACE" in line:
				acs = float(line.split()[0])
				acsArray.append(acs)

for e in acsArray:
	print(e)
