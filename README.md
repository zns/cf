# Co-translational Folding

## Available computers with graphics cards

```
(amber14) pirx.egr.duke.edu
(amber14) tichy.egr.duke.edu
(amber14) pemar-cmp-00.egr.duke.edu
pemar-cmp-01.egr.duke.edu
ed209.egr.duke.edu
```

## Requirements
- VMD 1.9.2
- AMBER14 and AMBER15Tools
- 
## To check

To check if a program is already running, run `ps aux | grep memd`.

To kill a running program, run `pkill -9 pmemd`



## Running

First clone the program:

```
git clone http://gitlab.oit.duke.edu/zns/cf.git 'mynewsimulation'
```

Then download the protein file you are trying to cotranslationally simulate and save it as `template.pdb`. 
Make sure to dump the protein file without any hydrgogens - in the VMD Tcl console simply do
```
set a [atomselect top "protein and noh"]
$a writepdb template.pdb
```
You may need to change some of the atom names (HSD / HIS) and remove the terminal oxygens.
Copy the FASTA amino acid sequence of the protein *for the structure that you save*.

First edit `conf.py` with your protein sequence and cotranslational method.

Then run:
```bash
nohup python simulate.py &
```

Then to watch the log file, use

```
tail -f simulation.log
```

## Visualizing in VMD

Go into a folder of the cotranslational folding (16/20/....) and then run
```
/usr/local/amber14/bin/cpptraj -p prmtop -i reimage.in
```
which will create `03_Prod_reimage.mdcrd`. 
Copy `prmtop` and `03_Prod_reimage.mdcrd` to your Desktop. Open VMD and load `prmtop` as `AMBER7 Parm`. Then load 
`03_Prod_reimage.mdcrd` as `AMBER Coordiantes with Periodic Box`.

## Collect RMSDs
```
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' > ../all.rmsd && cd ../20
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../24
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../28
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../32
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../36
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../40
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../44
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../48
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && cd ../52
sort -s -n -k 1,1 rmsds.dat | awk '{ print $2}' >> ../all.rmsd && xmgrace ../all.rmsd
```

## Making movies
Before you begin,

```
sudo apt-get install vlc pymol mencoder
```

Then to collect the data from remote host:

```
rsync -rv --include '*/' --include 'pdbs/*.pdb' --include "makeMovie.py" --exclude '*' --prune-empty-dirs user@host:folder ./
```

Then to make the movie file:

```
python3 makeMovie.py
```

This generates `movie.pdb`.

Open `movie.pdb` in PyMOL. Use the following to color PyMOL nicely:

```
bg_color white
set cartoon_discrete_colors, 1
set ray_trace_mode,  1
set ambient, 1
set reflect, 0
set two_sided_lighting, on
set cartoon_rect_width, 0.03
set cartoon_rect_length, 1.7
set cartoon_discrete_colors, on
```

Then change to a different type of view, if you want. 
Then "Save as a movie" and select "PNG". Make sure to save in a new folder.
Goto this folder and use the command:

```
mencoder -mc 0 -noskip -skiplimit 0 -ovc lavc -lavcopts vcodec=mpeg4:vhq:trell:mbd=2:vmax_b_frames=1:v4mv:vb_strategy=0:vlelim=0:vcelim=0:cmp=6:subcmp=6:precmp=6:predia=3:dia=3:vme=4:vqscale=1 "mf://*.png" -mf type=png:fps=120 -o ../output.avi
```

to encode it as `output.avi`.
